# Introduction to Jenkinsfile

_A presentation I gave at SELF 2019, cheat sheet, and sample Jenkinsfiles to kick start Jenkinsfile development._

This repo is where I store varios Jenkins and Jenkinsfile related files I've developed over the years. It likely won't update often, but hopefully what is here will be helpful. If there is something you'd like to add or imporve please add a pull request.

## File Descriptions
**SELF-19_Intro_to_Jenkinsfile.pdf**
> This is the PDF of the presentation given at South East Linux Fest (SELF) in June of 2019. This presenation was basically an introduction to Jenkins and writing a Jenkinsfile.

## Useful Jenkins Resources

* Declarative Syntax: https://jenkins.io/doc/book/pipeline/syntax/
* Snippet Generator: https://your.jenkins.server/pipeline-syntax
* Shared Libraries: https://jenkins.io/doc/book/pipeline/shared-libraries/
